CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Media Entity Download Filter module provides a Linkit CKEditor filter to
link to Media Entities in the ckeditor. This module searches for the
`field_file` on a media entity, then displays the download URI  of that media
entity instead of linking to the media detail page.

Note:
This feature is based on the `media_entity` contrib module. It is not compatible
with the media module in core since Drupal 8.4.

 * For a full description of the module visit:
   https://www.drupal.org/project/medf

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/medf


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Linkit - https://www.drupal.org/project/linkit


INSTALLATION
------------

 * Install the Media Entity Download Filter module as you would normally install
 a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
 further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and its
       dependencies.
    2. Navigate to Administration > Configuration > Content authoring > Text
       formats and editors, select a text format, with which you want to use
       this filter, and check "Link media direct to download url".
    3. Save.


MAINTAINERS
-----------

 * Jacob Bell (jacobbell84) - https://www.drupal.org/u/jacobbell84
 * Tassilo Groeper (tassilogroeper) - https://www.drupal.org/u/tassilogroeper
 * Rainer Friederich (yobottehg) - https://www.drupal.org/u/yobottehg

Supporting organization:

 * WONDROUS - https://www.drupal.org/wondrous
